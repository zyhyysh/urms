export default {
  actions: {
    async nuxtServerInit (context, payload) {
      // console.log('nuxtServerInit', context.state.admin.siteInfo)
      if (context.state.admin.siteInfo === null || context.state.admin.siteInfo === undefined) {
        const { status, data } = await this.$axios.get('/r/site_info')

        // console.log(status, data)

        if (status === 200) {
          context.commit('admin/updateSiteInfo', data[0])
          context.commit('admin/setLang', data[0].language || 'en')
        }
      }
    }
  }
}