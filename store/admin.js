import path from 'path'
import qs from 'querystring'
import Vue from 'vue'
import utils from '~~/assets/scripts/utils'

const errCodes = {
  404: 'Not Found',
  405: 'Not Allow',
  500: 'Server Error'
}

function response(status, payload) {
  if (status === 200) {
    return {
      success: true,
      data: payload,
      error: null
    }
  } else if (status === -1) {
    return {
      success: false,
      data: null,
      error: 'Invalid Payload'
    }
  } else if (status in errCodes) {
    return {
      success: false,
      data: null,
      error: errCodes[status]
    }
  } else {
    return {
      success: false,
      data: null,
      error: `[Error] ${payload}`
    }
  }
}

export default {
  state () {
    return {
      locale: 'en',
      siteInfo: null,
      contextActionHandlers: {},
      resourceOptions: {},
      contentEntities: {},
      loginUser: {}
    }
  },
  mutations: {
    setLang (state, payload) {
      state.locale = payload
    },
    updateSiteInfo (state, payload) {
      Vue.set(state, 'siteInfo', payload)
    },
    updateContextAction (state, { key, action }) {
      if (action === undefined) {
        Vue.delete(state.contextActionHandlers, key)
      } else {
        Vue.set(state.contextActionHandlers, key, action)
      }
    },
    updateResourceOption (state, { options }) {
      // console.log(options)
      state.resourceOptions = options
    },
    updateOptionOfResource (state, { name, option }) {
      Vue.set(state.resourceOptions, name, option)
    },
    updateContentEntities (state, { name, entities }) {
      Vue.set(state.contentEntities, name, entities)
    },
    updateLoginStatus (state, user) {
      if (user) {
        Vue.set(state, 'loginUser', user)
      } else {
        Vue.set(state, 'loginUser', {})
      }
    }
  },
  actions: {
    registerContextActions ({ commit }, payload) {
      Object.keys(payload).forEach(key => {
        const action = payload[key]

        commit('updateContextAction', { key, action })
      })
    },
    unregisterContextActions ({ commit, state }, payload) {
      if (utils.getType(payload) === 'array') {
        payload.forEach(key => {
          commit('updateContextAction', { key, action: undefined })
        })
      } else if (utils.getType(payload) === 'string') {
        commit('updateContextAction', { key: payload, action: undefined })
      } else {
        Object.keys(state.contextActionHandlers).forEach(key => {
          commit('updateContextAction', { key, action: undefined })
        })
      }
    },
    async fetchResourceOptions ({ commit }) {
      try {
        const r = await this.$axios.get('/admin/model?system=true')

        if (r.status === 200) {
          commit('updateResourceOption', { options: r.data })
        } 
      
        return r
      } catch (e) {
        console.error(e)

        return e.response
      }
    },
    async fetchResourceOptionByName ({ commit }, payload) {
      const name = payload.name
      // if (name && name.length > 0) {
      try {
        return await this.$axios.get(`/admin/model/${name}`)
      } catch (e) {
        console.error(e)

        return e.response
      }

        // if (status === 200) {
        //   // commit('updateOptionOfResource', { name, option: data })

        //   return response(true, data)
        // }
        // response(status, data)
      // }

      // return response(-1)
    },
    async fetchContentModels ({ commit }) {
      try {
        return await this.$axios.get('/admin/model')
      } catch (e) {
        console.error(e)

        return e.response
      }

      // return response(status, data)
    },
    async deleteResourceOption ({ commit }, payload) {
      const name = payload.name

      try {
        return await this.$axios.delete(`/admin/model/${name}`)
      } catch (e) {
        return {
          status: e.response.status,
          error: e.response.data.errors.cover
        }
      }
    },
    async fetchContentEntities ({ commit }, payload) {
      const name = payload.name
      const query = payload.query || null
      // if (name && name.length > 0) {
      let url = `/r/${name}`

      if ('query' in payload && payload.query) {
        url += `?${qs.stringify(payload.query)}`
      }

      try {
        const r = await this.$axios.get(url)

        if (r.status === 200) {
          // commit('updateContentEntities', { name, entities: r.data })
        }

        return r
      } catch (e) {
        console.error(e)

        return e.response
      }

        // return response(status, data)
      // }

      // return response(-1)
    },
    async fetchContentEntityById ({ commit }, payload) {
      const name = payload.name
      const id = payload.id
      // if (name && name.length > 0 && id && id.length > 0) {
      try {
        return await this.$axios.get(`/r/${name}/${id}`)
      } catch (e) {
        console.error(e)

        return e.response
        // return response(status, data)
      }
      // }

      // return response(-1)
    },
    async saveContent ({ comment }, { name, id, entity }) {
      // if (name && name.length > 0 && entity) {
      try {
        return await (async () => {
          if (id) {
            return await this.$axios.put(`/r/${name}/${id}`, entity)
          } else {
            return await this.$axios.post(`/r/${name}`, entity)
          }
        })()
      } catch (e) {
        console.error(e)
        // console.dir(e.response)
        console.log(e.response.data)

        return {
          status: e.response.status,
          error: e.response.data
        }
        // return response(status, data)
      }
      // }

      // return response(-1)
    },
    async deleteContent ({}, { name, id }) {
      // if (name && name.length > 0 && id && id.length > 0) {
      try {
        return await this.$axios.delete(`/r/${name}/${id}`)
      } catch (e) {
        console.error(e)

        return {
          status: e.response.status,
          error: e.response.data.errors.cover
        }
      }

        // return response(status)
      // }

      // return response(-1)
    },
    /* Login */
    async login ({ commit }, { username, password }) {
      console.log(username, password)
      try {
        const r = await this.$axios.post('/admin/login', { username, password })

        if (r.status === 200) {
          this.$axios.defaults.headers.common['Authorization'] = r.data.token
          commit('updateLoginStatus', r.data)
        }

        return r
      } catch (e) {
        console.error(e)
        this.$axios.defaults.headers.common['Authorization'] = ''
        return e.response
      }
    }
  }
}
