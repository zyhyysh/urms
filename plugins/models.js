import Vue from 'vue'

import CustomEditors from '~~/model_plugins/index.js'

Vue.use(CustomEditors)