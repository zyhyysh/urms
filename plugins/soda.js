import Vue from 'vue'

import soda from '~~/components/soda/index.js'
import utils from '~~/assets/scripts/utils.js'
// import BemMixin from '~~/assets/scripts/mixins/BemMixin.js'

Vue.use(soda)
// Vue.mixin(BemMixin)

function calculateClasses (block, elem, mods) {
  const bemClass = Object.keys(elem).length > 0 ? `${block}__${Object.keys(elem)[0]}` : block

  if (utils.getType(mods) === 'object') {
    return [bemClass].concat(
      Object.keys(mods).filter(m => {
        return Boolean(mods[m])
      }).map(m => {
        return `${bemClass}--${m}`
      })
    )
  }

  return [bemClass]
}

Vue.directive('bem', function (el, binding) {
  const oldList = calculateClasses(binding.arg, binding.modifiers, binding.oldValue)
  const newList = calculateClasses(binding.arg, binding.modifiers, binding.value)
  const removeList = oldList.filter(c => newList.indexOf(c) === -1)

  newList.forEach(c => el.classList.add(c))
  removeList.forEach(c => el.classList.remove(c))
})