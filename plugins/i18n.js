import Vue from 'vue'
import VueI18n from 'vue-i18n'
import en from '~~/locales/en.js'
import zh from '~~/locales/zh.js'

Vue.use(VueI18n)

export default ({ app, store }) => {
  app.i18n = new VueI18n({
    locale: store.state.admin.locale,
    fallbackLocale: 'en',
    messages: {
      en,
      zh
    }
  })
}