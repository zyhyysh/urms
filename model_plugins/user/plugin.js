const crypto = require('crypto')

module.exports = {
  api: {
    async beforeCreate (ctx) {
      let body = ctx.request.body

      if (!('pass' in body)) {
        throw 'Password Required'
      } else if (!('validatepass' in body)) {
        throw 'Password Validation Required'
      } else if (body.pass !== body.validatepass) {
        throw 'Incorrect Password'
      }

      return body
    },
    async beforeUpdate (ctx, something, doc) {
      console.log('beforeUpdate')
      if (!('validatepass' in ctx.request.body)) {
        throw 'Password Validation Required'
      }
      const hash = crypto.createHash('sha256')
      const oldpass = hash.update(ctx.request.body.validatepass).digest('hex')
      const docpass = doc.hash

      console.log(oldpass, docpass)

      if (oldpass === docpass) {
        return doc
      } else {
        throw 'Incorrect password'
      }
    },
    async beforeReplace (ctx, something, doc) {
      console.log('beforeReplace')
      if (!('validatepass' in ctx.request.body)) {
        throw 'Password Validation Required'
      }
      const hash = crypto.createHash('sha256')
      const oldpass = hash.update(ctx.request.body.validatepass).digest('hex')
      const docpass = doc.hash

      console.log(oldpass, docpass)

      if (oldpass === docpass) {
        return doc
      } else {
        throw 'Incorrect password'
      }
    },
    async beforeResponse (doc) {
      return doc.safeObj
    }
  },
  schema (schema) {
    function passToHash (next) {
      // console.log('passToHasn1', this.pass, this.hash)
      if (this.pass.length > 0 && this.pass !== 'hashed') {
        const hash = crypto.createHash('sha256')
        const passHash = hash.update(this.pass)
        this.hash = passHash.digest('hex')

        // console.log('passToHash2', this.pass, this.hash)
        this.pass = 'hashed'
      }

      next()
    }

    schema.pre('save', passToHash)
    // schema.pre('update', passToHash)

    const safeObj = schema.virtual('safeObj')

    safeObj.get(function () {
      const obj = this.toObject()
      delete obj['pass']
      delete obj['hash']

      return obj
    })
  }
}