const axios = require('axios')

module.exports = {
  api: {
    async afterIndex (ctx, list) {
      // console.log('client_infos afterIndex', list)
      async function getAqi () {
        try {
          const { status, data } = await axios.get('http://api.waqi.info/feed/beijing/?token=f9686c19c6f124f2c561eacb6bc76d2f75c04c3c')
          // console.log('getaqi', status, data)

          if (status === 200) {
            return data.data.aqi
          } else {
            throw data
          }
        } catch (e) {
          throw e
        }
      }

      if (list && list.length > 0) {
        const a = list[0]
        const expiration = a.aqi['expiration']
        const lastUpdated = a.aqi['last_updated']

        if (lastUpdated.getTime() + expiration >= Date.now()) {
          return [a]
        }

        try {
          const aqi = await getAqi()
          a.aqi.value = aqi
          a.aqi.last_updated = new Date()

          await a.save()

          return [a]
        } catch (e) {
          throw e
        }
      } else {
        try {
          const aqi = await getAqi()
          const a = await this.model.create({
            aqi: {
              value: aqi,
              last_updated: new Date(),
              expiration: 60 * 60 * 1000
            }
          })

          // console.log('a', a)

          const r = await a.save()

          return [r]
        } catch (e) {
          throw e
        }
      }     
    }
  }
}