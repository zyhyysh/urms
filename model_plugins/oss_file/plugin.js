const OSS = require('ali-oss')

module.exports = {
  api: {
    parseQuery (ctx) {
      let q = Object.assign({}, ctx.query)
      console.log('plugin', q, '')

      if ('path' in q) {
        try {
          q.path = { $eq : q.path }
        } catch (e) {
          console.error(e)
          throw e
        }
      } else {
        q['path'] = { $eq : '/' }
      }

      console.log('plugin after', q)

      return q
    },
    afterDelete (ctx) {
      // const 
    }
  }
}