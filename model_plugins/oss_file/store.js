export default {
  state () {
    return {
      'AccessKeyId': '',
      'AccessKeySecret': '',
      'SecurityToken': '',
      'Expiration': '',
      endpoint: '',
      bucket: '',
      requesting: false
    }
  },
  getters: {
    isRequesting (state) {
      return state.requesting
    },
    hasExpired (state) {
      return () => state['Expiration'].length === 0 ||
             (new Date(state['Expiration'])).getTime() - Date.now() <= 0
    },
    canUpload (state) {
      return state['AccessKeyId'].length > 0 &&
             state['AccessKeySecret'].length > 0 &&
             state['SecurityToken'].length > 0
    },
    clientOption (state) {
      return {
        endpoint: state.endpoint,
        bucket: state.bucket,
        accessKeyId: state.AccessKeyId,
        accessKeySecret: state.AccessKeySecret,
        stsToken: state.SecurityToken
      }
    }
  },
  mutations: {
    toggleRequesting (state, payload) {
      state.requesting = payload
    },
    updateAss (state, payload) {
      ['AccessKeyId', 'AccessKeySecret', 'SecurityToken', 'Expiration', 'endpoint', 'bucket'].forEach(k => {
        state[k] = payload[k]
      })
    }
  },
  actions: {
    async fuckAss ({ state, commit }) {
      if (!state.requesting) {
        commit('toggleRequesting', true)
        const { status, data } = await this.$axios.get('/r/aliyunoss')
        commit('toggleRequesting', false)

        if (status === 200) {
          commit('updateAss', data)
        }
      }
    }
  }
}