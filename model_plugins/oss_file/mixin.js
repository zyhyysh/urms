import OSS from 'ali-oss'
import { mapGetters, mapActions } from 'vuex'

export default {
  computed: {
    ...mapGetters({
      hasExpired: 'oss_file/hasExpired',
      canUpload: 'oss_file/canUpload',
      clientOption: 'oss_file/clientOption'
    })
  },
  methods: {
    ...mapActions({
      fuckAss: 'oss_file/fuckAss'
    }),
    async uploadOssFile(file, fname, client = {}, checkpoint = undefined, progress = () => {}) {
      if (this.hasExpired) {
        await this.fuckAss()
      }

      client = new OSS.Wrapper(this.clientOption)

      try {
        if (!file) { 
          throw 'no file'
        }

        return await (new Promise((resolve, reject) => {
          client.multipartUpload(fname, file, {
            progress,
            partSize: 100 * 1024,
            checkpoint
          }).then(({ res }) => {
            if (res.status === 200) {
              checkpoint = null
              client = null
              resolve(res.requestUrls[0].split('?')[0])
            }
          }).catch(err => {
            reject(err)
          })
        }))
      } catch (e) {
        console.error(e)
        progress(-0.01, null)
        throw e
      }
    },
    async deleteOssFile (objectKey) {
      console.log('deleteOssFile', objectKey)
      if (this.hasExpired) {
        await this.fuckAss()
      }

      let client = new OSS.Wrapper(this.clientOption)

      try {
        return await (new Promise((resolve, reject) => {
          client.delete(objectKey).then(res => {
            resolve(res)
          }).catch(e => {
            reject(e)
          })
        }))
      } catch (e) {
        console.error(e)
        throw e
      }
    }
  }
}