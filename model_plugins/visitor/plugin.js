module.exports = {
  api: {
    async beforeCreate(ctx) {
      console.log('create visitor')
      let body = ctx.request.body

      body['ip_addr'] = ctx.request.ip

      return body
    },
    parseQuery (ctx) {
      let q = Object.assign({}, ctx.query)
      let query = {}
      console.log('query', q)

      if ('time.before' in q || 'time.after' in q) {
        let qTime = {}
        let timeAfter = Number(q['time.after'])
        let timeBefore = Number(q['time.before'])

        if (timeAfter) {
          const d = new Date(timeAfter)
          console.log(timeAfter, d)
          qTime['$gte'] = new Date(timeAfter)
        }

        if (timeBefore) {
          qTime['$lte'] = timeBefore
        }

        query['records'] = { '$elemMatch': { time_enter: qTime } }
      }

      if ('path' in q || 'path.match' in q) {
        let qPath = q.path ? q.path : { $regex: q['path.match'] }

        query['records'] = { '$elemMatch': { path: qPath } }
      }

      console.dir(query)

      return query
    }
  }
}