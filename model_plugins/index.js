/* Mixin */
import EditorMixin from '~~/assets/scripts/mixins/EditorMixin.js'
import ListMixin from '~~/assets/scripts/mixins/ListMixin.js'
import ListItemMixin from '~~/assets/scripts/mixins/ListItemMixin.js'

/* Default Editor */
import EntityEditorString from '~~/components/editors/EntityEditorString'
import EntityEditorNumber from '~~/components/editors/EntityEditorNumber'
import EntityEditorDate from '~~/components/editors/EntityEditorDate'
import EntityEditorBoolean from '~~/components/editors/EntityEditorBoolean'
import EntityEditorArray from '~~/components/editors/EntityEditorArray'
import EntityEditorObject from '~~/components/editors/EntityEditorObject'
import EntityEditorRefWrapper from '~~/components/editors/EntityEditorRefWrapper'
import EntityEditorBuffer from '~~/components/editors/EntityEditorBuffer'
import EntityEditorMixed from '~~/components/editors/EntityEditorMixed'

/* Default List */
import EntityList from '~~/components/EntityList'

/* Default List Item */
import EntityListItem from '~~/components/EntityListItem'

/* Default Picker */
import EntityPicker from '~~/components/EntityPicker'

/* Custom Editor */
import OssFileEditor from './oss_file/editor.vue'
import ProjectEditor from './project/editor.vue'
import NewsEditor from './news/editor.vue'
import AboutEditor from './about/editor.vue'
import JMLArticleEditor from './jml_article/editor.vue'
import MDArticleEditor from './md_article/editor.vue'

/* Custom List */
import TagList from './tag/list.vue'
import OssFileList from './oss_file/list.vue'
import AboutList from './about/list.vue'

/* Custom List Item */
import OssFileItem from './oss_file/item.vue'
import TagItem from './tag/item.vue'
import ProjectItem from './project/item.vue'
import NewsItem from './news/item.vue'
import AboutItem from './about/item.vue'

/* Settings */
import UserSetting from './user/setting.vue'
import SiteInfoSetting from './site_info/setting.vue'
import ClientInfo from './client_info/index.vue'
import Visitor from './visitor/index.vue'

export default {
  defaultEditors: {
    EntityEditorString,
    EntityEditorNumber,
    EntityEditorDate,
    EntityEditorBoolean,
    EntityEditorArray,
    EntityEditorObject,
    EntityEditorRefWrapper,
    EntityEditorBuffer,
    EntityEditorMixed
  },
  customEditors: {
    'oss_file': OssFileEditor,
    'project': ProjectEditor,
    'news': NewsEditor,
    'about': AboutEditor,
    'jml_article': JMLArticleEditor,
    'md_article': MDArticleEditor
  },
  customLists: {
    'tag': TagList,
    'oss_file': OssFileList,
    'about': AboutList
  },
  customListItems: {
    'oss_file': OssFileItem,
    'tag': TagItem,
    'project': ProjectItem,
    'news': NewsItem,
    'about': AboutItem
  },
  customSettings: {
    'user': UserSetting,
    'site_info': SiteInfoSetting,
    'client_info': ClientInfo,
    'visitor': Visitor
  },
  hasCustomEditor (name) {
    return Object.keys(this.customEditors).indexOf(name) !== -1
  },
  hasCustomList (name) {
    return Object.keys(this.customLists).indexOf(name) !== -1
  },
  hasCustomListItem (name) {
    return Object.keys(this.customListItems).indexOf(name) !== -1
  },
  hasCustomSetting (name) {
    return Object.keys(this.customSettings).indexOf(name) !== -1
  },
  install (Vue, options) {
    /* 
    ** register default editors 
    */
    Object.keys(this.defaultEditors).forEach(key => {
      let c = this.defaultEditors[key]
      if ('mixins' in c) {
        c.mixins.push(EditorMixin)
      } else {
        c['mixins'] = [EditorMixin]
      }
      
      Vue.component(key, this.defaultEditors[key])
    })

    /* 
    ** register default list 
    */
    Vue.component(EntityList.name, EntityList)

    /* 
    ** register default list item
    */
    Vue.component(EntityListItem.name, EntityListItem)

    /* 
    ** register default picker
    */
    Vue.component(EntityPicker.name, EntityPicker)


    /* 
    ** register custom editors 
    */
    Object.keys(this.customEditors).forEach(key => {
      let c = this.customEditors[key]

      // console.log(key, c.mixins)
      if ('mixins' in c) {
        c.mixins.push(EditorMixin)
      } else {
        c['mixins'] = [EditorMixin]
      }
      // console.log(key, c.mixins)

      Vue.component(`CustomEditor_${key}`, c)
    })
    Vue.prototype.$hasCustomEditor = Vue.hasCustomEditor = name => this.hasCustomEditor(name)


    /* 
    ** register custom lists 
    */
    Object.keys(this.customLists).forEach(key => {
      let c = this.customLists[key]

      if ('mixins' in c) {
        c.mixins.push(ListMixin)
      } else {
        c['mixins'] = [ListMixin]
      }

      Vue.component(`CustomList_${key}`, c)
    })
    Vue.prototype.$hasCustomList = Vue.hasCustomList = name => this.hasCustomList(name)


    /* 
    ** register custom items 
    */
    Object.keys(this.customListItems).forEach(key => {
      let c = this.customListItems[key]

      if ('mixins' in c) {
        c.mixins.push(ListItemMixin)
      } else {
        c['mixins'] = [ListItemMixin]
      }

      Vue.component(`CustomListItem_${key}`, c)
    })
    Vue.prototype.$hasCustomListItem = Vue.hasCustomListItem = name => this.hasCustomListItem(name)


    /*
    ** register custom settings
    */
    Object.keys(this.customSettings).forEach(key => {
      let c = this.customSettings[key]

      if ('mixins' in c) {
        c.mixins.push(ListMixin)
      } else {
        c['mixins'] = [ListMixin]
      }

      Vue.component(`CustomSetting_${key}`, c)
    })
    Vue.prototype.$hasCustomSetting = Vue.hasCustomSetting = name => this.hasCustomSetting(name)
  }
}