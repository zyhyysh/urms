import _ from 'lodash'
import utils from '~~/assets/scripts/utils.js'

export default {
  isElement (jml) {
    return utils.getType(jml) === 'array'
  },
  isAttr (jml) {
    return utils.getType(jml) === 'object'
  },
  isText (jml) {
    return utils.getType(jml) === 'string'
  },
  isBlockElem (jml) {
    return ['div', 'p', 'section', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'ul', 'ol', 'li', 'blockquote', 'figure', 'table', 'header', 'hgroup', 'hr', 'video'].indexOf(this.getNodeName(jml)) !== -1
  },
  getNodeName (jml) {
    if (this.isText(jml)) {
      return 'text'
    } else if (this.isElement(jml)) {
      return jml[0]
    }

    return ''
  },
  hasAttr (jml) {
    return this.isElement(jml) && this.isAttr(jml[1])
  },
  getAttr (jml) {
    if (this.hasAttr(jml)) {
      return Object.assign({}, jml[1])
    } else {
      return {}
    }
  },
  setAttr (jml, attr) {
    console.log('set attr', jml, attr)
    // const hasAttr = this.hasAttr(jml)
    const oriAttr = this.getAttr(jml)
    attr = _.merge(oriAttr, attr)
    // if (hasAttr) {
    //   jml[1] = attr
    // } else {
    //   jml = jml.slice(0, 1).concat([attr], jml.slice(1))
    // }

    return [this.getNodeName(jml), attr].concat(this.getChildren(jml))
  },
  removeAttr (jml, key) {
    if (this.hasAttr(jml)) {
      // const oriAttr = this.getAttr(jml)

      // if (key in oriAttr) {
      //   delete oriAttr[key]
      // }

      delete jml[1][key]
    }

    return jml
  },
  hasChild (jml) {
    return this.isElement(jml) && (this.hasAttr(jml) ? jml.length > 2 : jml.length > 1)
  },
  getChildren (jml) {
    if (this.hasChild(jml)) {
      if (this.hasAttr(jml)) {
        return jml.slice(2)
      } else {
        return jml.slice(1)
      }
    } else {
      return []
    }
  },
  insertChild (jml, child, atIdx) {
    // let elem = this.hasAttr(jml) ? jml.slice(0, 2) : jml.slice(0, 1)
    let children = this.getChildren(jml)
    children = children.slice(0, atIdx).concat([child], children.slice(atIdx))
    return this.replaceChildren(jml, children)
  },
  replaceChild (jml, child, atIdx) {
    // let elem = this.hasAttr(jml) ? jml.slice(0, 2) : jml.slice(0, 1)
    let children = this.getChildren(jml)
    children = children.slice(0, atIdx).concat([child], children.slice(atIdx + 1))
    return this.replaceChildren(jml, children)
  },
  appendChild (jml, child) {
    return jml.concat([child])
  },
  prependChild (jml, child) {
    return this.insertChild(jml, child, 0)
  },
  removeChild (jml, idx) {
    let children = this.getChildren(jml)
    children = children.slice(0, idx).concat(children.slice(idx + 1))
    return this.replaceChildren(jml, children)
  },
  replaceChildren (jml, children) {
    // console.log('replaceChildren', jml, children)
    // let elem = jml[0].concat(this.getAttr(jml))
    return [this.getNodeName(jml), this.getAttr(jml)].concat(children)
  },
  splitAt (jml, indexs) {
    // nodeIdx += this.hasAttr(jml) ? 2 : 1
    // console.log('splitAt start', indexs)
    // console.log('isText', this.isText(jml))
    const idx = indexs.shift()
    // console.log(idx)

    if (this.isText(jml)) {
      return [
        jml.slice(0, idx),
        jml.slice(idx)
      ]
    } else if (this.isElement(jml)) {
      const children = this.getChildren(jml)
      let beforeChildren = children.slice(0, idx)
      let splitChildren = children[idx]
      let afterChildren = children.slice(idx + 1).slice()

      if (indexs.length > 0) {
        let parts = this.splitAt(splitChildren, indexs)

        // console.log('s children', children)
        // console.log('spliting', parts[0], parts[1])
        // console.log('splittt', beforeChildren, splitChildren, afterChildren)

        return [
          this.replaceChildren(jml, beforeChildren.concat([parts[0]])),
          this.replaceChildren(jml, [parts[1]].concat(afterChildren))
        ]
      }

      return [
        this.replaceChildren(jml, beforeChildren.concat(splitChildren)),
        this.replaceChildren(jml, afterChildren)
      ]
      // console.log('splitAt end')
    }
  },
  fromDom(dom) {

  }
}