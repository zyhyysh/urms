export default {
  login: '登录',
  logout: '退出',
  username: '用户名',
  password: '密码',
  'login to': '登录 {msg}',
  'redirect from': '重定向自 {msg}',
  action: {
    save: '保存',
    save_c: '是否保存 {content}？',
    delete: '删除',
    delete_c: '确认删除 {content}？',
    publish: '发布',
    unpublish: '撤回',
    reload: '重载',
    preview: '预览',
    upload: '上传',
    download: '下载',
    edit: '编辑',
    filter: '筛选',
    add: '添加',
    cancel: '取消',
    select_all: '全选',
    unselect_all: '全不选',
    select_item: '选择{num}个项目'
  },
  content: {
    list: '列表',
    new: '新建',
    type_here: '在此输入...',
    type_str_0: '在此输入',
    type_str_min: '不少于{num}',
    type_str_and: '并且',
    type_str_max: '不多于{num}',
    type_str_char: '个字...',
    type_str_1: '...'

  }
}