export default {
  login: 'login',
  logout: 'logout',
  username: 'username',
  password: 'password',
  'login to': 'login to {msg}',
  'redirect from': 'redirect from {msg}',
  action: {
    save: 'save',
    save_c: 'save {content}?',
    delete: 'delete',
    delete_c: 'delete {content}?',
    publish: 'publish',
    unpublish: 'unpublish',
    reload: 'reload',
    preview: 'preview',
    upload: 'upload',
    download: 'download',
    edit: 'edit',
    filter: 'filter',
    add: 'add',
    cancel: 'cancel',
    select_all: 'select all',
    unselect_all: 'unselect all',
    select_item: 'select one items' | 'select {num} items'
  },
  content: {
    list: 'list',
    new: 'new entity',
    type_here: 'type here...',
    type_str_0: 'type',
    type_str_min: 'more than {num}',
    type_str_and: 'and',
    type_str_max: 'less than {num}',
    type_str_char: 'charactors...',
    type_str_1: 'here...'
  }
}