const pkg = require('./package')
const path = require('path')
const createResolver = require('postcss-import-resolver')

module.exports = {
  mode: 'universal',
  manifest: {
    name: 'Uniform Resource Manager System'
  },

  /*
  ** Headers of the page
  */
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#22C' },

  /*
  ** Global CSS
  */
  css: [
    // { src: '~/assets/styles/main.pcss', lang: 'postcss' }
  ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~~/plugins/i18n.js',
    '~~/plugins/soda.js',
    '~~/plugins/models.js',
    { src: '~~/plugins/datepicker.js', ssr: false }
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://github.com/nuxt-community/axios-module#usage
    '@nuxtjs/auth',
    '@nuxtjs/axios',
    '@nuxtjs/toast'
  ],

  /*
  ** Axios module configuration
  */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
    baseURL: 'http://127.0.0.1:3333/api',
    browserBaseURL: '/api',
    // credentials: false
  },

  /*
  ** Auth module configuration 
  */
  auth: {
    redirect: {
      login: '/admin/login',
      logout: '/admin/login',
      home: '/admin',
      user: '/admin'
    },
    strategies: {
      local: {
        endpoints: {
          login: {
            url: '/admin/auth',
            method: 'post',
            propertyName: 'token'
          },
          logout: {
            url: '/admin/auth',
            method: 'delete'
          },
          user: {
            url: '/admin/auth',
            method: 'get',
            propertyName: 'user'
          }
        }
      }
    }
  },
  /*
  ** Toast module configuration
  */
  toast: {
    position: 'bottom-right',
    duration: 10000,
    theme: 'outline',
    className: 'default-toast'
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }

      config.resolve.alias = Object.assign(config.resolve.alias, {
        '~': path.join(this.options.srcDir, 'client')
      })
    },
    /* POSTCSS */
    postcss: {
      plugins: {
        'postcss-import': {
          resolve: createResolver({
            alias: {
              '~~': path.join(__dirname),
              '~': path.join(__dirname, 'client')

            }
          })
        },
        'postcss-url': {},
        'postcss-cssnext': {},
        'postcss-nested': {}
      }
    },
    publicPath: '/_urms/'
  },
  /* Middleware */
  router: {
    middleware: ['auth']
  },
  env: {
    // baseUrl: 'http://localhost:3333/folio'
    // baseUrl: process.env.BASE_URL ? path.join(process.env.BASE_URL, '/folio') : 'http://localhost:3333/folio'
  }
}
