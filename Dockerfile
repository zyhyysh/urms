FROM node:10.0.0

ENV HOST 0.0.0.0

EXPOSE 3333

RUN mkdir -p /app
COPY . /app
WORKDIR /app

RUN npm config set registry https://registry.npm.taobao.org
RUN npm install
RUN npm run build

CMD ["npm", "start"]
