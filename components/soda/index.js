import SDBar from './SDBar'
import SDBox from './SDBox'
import SDCarbinet from './SDCarbinet'
import SDSelector from './SDSelector'
import SDOption from './SDOption'
import SDToggle from './SDToggle'
import SDModalStage from './SDModalStage'
import SDSegmentSwitch from './SDSegmentSwitch'
import SDNotification from './SDNotification'
import SDNotificationCenter from './SDNotificationCenter'
import SDAlert from './SDAlert'

export default {
  sdOptions: {
    SDBox,
    SDBar, 
    SDCarbinet, 
    SDSelector, 
    SDOption, 
    SDToggle, 
    SDModalStage, 
    SDSegmentSwitch, 
    SDNotification, 
    SDAlert
  },
  components: {},
  install (Vue, options) {
    Object.keys(this.sdOptions).forEach(key => {
      this.components[key] = Vue.component(key, this.sdOptions[key])
    })

    const SDNotificationComp = this.components['SDNotification']

    let notificationCenter = new SDNotificationCenter(Vue, SDNotificationComp)

    Vue.prototype.$notify = Vue.notify = (options) => {
      options = Object.assign({
        title: null,
        message: null,
        context: 'default',
        duration: 5000,
        actions: []
      }, options)

      console.log('vue root', Vue)
      if ((options.title && options.title.length > 0) ||
          (options.message && options.message.length > 0)) {
        notificationCenter.push(options)
      }
    }
  }
}