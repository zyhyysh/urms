export default class SDNotificationCenter {
  constructor (Vue, comp) {
    this.comp = comp
    this.notifications = {}
    this.$hub = null
  }
  push (notification)  {
    if (this.$hub === null) {
      this.$hub = document.createElement('div')
      this.$hub.setAttribute('id', 'sd-notification-hub')
      this.$hub.className = 'sd-notification-hub'
      document.body.appendChild(this.$hub)
    }

    const key = `${notification.title}${notification.message}${Date.now()}`
    this.notifications[key] = notification

    if ('Notification' in window) {
      if (Notification.permission === 'denied') {
        this.showSDNotification(key, notification)
      } else if (Notification.permission === 'granted') {
        const n = new Notification(notification.title.toUpperCase(), {
          body: notification.message || ''
        })
      } else {
        Notification.requestPermission((p) => {
          if (!'permission' in Notification) {
            Notification.permission = p
          }
        })
        console.log('permission is', Notification.permission)
      }
    } else {
      console.log('not support notification')
      this.showSDNotification(key, notification)
    }
  }
  showSDNotification (key, notification) {
    const $n = new this.comp({
      propsData: {
        nid: key,
        title: notification.title,
        message: notification.message,
        context: notification.context,
        actions: notification.actions
      }
    }).$mount()

    $n.$once('dismiss', () => {
      this.dismiss(key)
    })

    console.log($n.$el)

    this.$hub.appendChild($n.$el)

    if (notification.duration > 0) {
      setTimeout(() => {
        this.dismiss(key)
      }, notification.duration)
    }
  }
  dismiss (key) {
    delete this.notifications[key]
    document.getElementById(key).remove()
  }
}