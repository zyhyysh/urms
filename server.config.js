module.exports = {
  port: 3333,
  api: {
    prefix: '/r',
    resourceOptions: './options',
    resourcePlugins: '../model_plugins',
  },
  db: {
    user: '',
    pass: '',
    host: 'db',
    port: 27017,
    database: 'test_urms',
    // host: '101.201.103.104',
    // port: 3010,
    // database: 'fcjz',
    schema: './options'
  },
  alioss: {
    accessKeyId : 'LTAIUAe6pmnzl9ZG',
    accessKeySecret : 'ooHPs99dOChVNIr30PkiCjSGURBSFF',
    role : 'acs:ram::30231133:role/oss-sts',
    bucket : 'fcjzstoyard',
    endpoint : 'http://oss-cn-beijing.aliyuncs.com',
    // 'TokenExpireTime' : '3600',
    roleSessionName : 'external-username',
    policy: {
      'Statement': [
        {
          'Action': [
            'oss:GetObject',
            'oss:PutObject',
            'oss:DeleteObject',
            'oss:ListParts',
            'oss:AbortMultipartUpload',
            'oss:ListObjects'
          ],
          'Effect': 'Allow',
          'Resource': ['acs:oss:*:*:*']
        }
      ],
      'Version': '1'
    }
  }
}