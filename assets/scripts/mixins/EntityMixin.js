import utils from '~~/assets/scripts/utils'

export default {
  props: {
    /*
    ** Schema of Entity
    */
    schema: {
      type: Object,
      require: true
    }
  },
  computed: {
    ignoreKeys () {
      return [
        'meta', 
        'comment',
        'editor'
      ]
    },
    isItem () {
      return 'type' in this.schema
    },
    isArray () {
      return this.isItem && utils.getType(this.schema.type) === 'array'
    },
    schemaType () {
      if (this.isArray) {
        return 'Array'
        // return this.schema.type[0].type
      } else if (this.isItem) {
        return this.schema.type
      } else {
        return 'Object'
      }
    }
  }
}