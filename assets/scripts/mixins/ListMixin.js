const orderMethods = {
  lastModTime: (dir) => {
    return (a, b) => {
      const timeA = new Date(a.meta.modifications[a.meta.modifications.length - 1].time)
      const timeB = new Date(b.meta.modifications[b.meta.modifications.length - 1].time)

      return dir * (timeB.getTime() - timeA.getTime())
    }
  },
  createTime: (dir) => {
    return (a, b) => {
      const timeA = new Date(a.meta.modifications[0].time)
      const timeB = new Date(b.meta.modifications[0].time)

      return dir * (timeAB.getTime() - timeA.getTime())
    }
  }
}

export default {
  props: {
    fnAdd: {
      type: Boolean,
      default: false
    },
    fnDelete: {
      type: Boolean,
      default: false
    },
    fnEdit: {
      type: Boolean,
      default: false
    },
    fnPick: {
      type: Boolean,
      default: false
    },
    fnResort: {
      type: Boolean,
      default: false
    },
    fnPage: {
      type: Boolean,
      default: false
    },
    list: {
      type: Array,
      required: true
    },
    schema: {
      type: Object
    },
    name: {
      type: String,
      default: ''
    },
    query: {
      type: Object
    },
    selectedList: {
      type: Array,
      default: () => []
    }
  },
  data: () => ({
    orderMethod: ['lastModTime', 1],
    perPageItemCount: 24,
    currentPageIdx: 0,
    onResorting: false,
    reSortContext: {
      item: null,
      fromIdx: -1,
      toIdx: -1
    },
    zoomSize: 1,
    zoomMax: 5,
  }),
  computed: {
    hasCustomItem () {
      return this.$hasCustomListItem(this.name)
    },
    selectedIds () {
      return this.selectedList ? this.selectedList.map(i => i._id) : []
    },
    sortFunc () {
      return orderMethods[this.orderMethod[0]](this.orderMethod[1])
    },
    sortedList () {
      if (this.fnResort) {
        return this.list
      } else {
        return this.list.slice().sort(this.sortFunc)
      }
    },
    pageCount () {
      if (this.fnPage && !this.fnResort) {
        return Math.ceil(this.list.length / this.perPageItemCount)
      } else {
        return 1
      }
    },
    pagedList () {
      if (this.fnPage && !this.fnResort) {
        const start = this.currentPageIdx * this.perPageItemCount
        const end = start + this.perPageItemCount
        return this.sortedList.slice(start, end)
      } else {
        return this.sortedList
      }
    }
  },
  methods: {
    onAddChild (entity) {
      this.$emit('add', { entity })
    },
    onChildUpdate (entity) {
      this.$emit('update', { entity })
    },
    onChildSelect (entity) {
      this.$emit('select', { entity })
    },
    onChildDelete (entity) {
      if (this.fnDelete) {
        this.$emit('delete', { entity })
      }
    },
    onQueryUpdate (query) {
      this.$emit('query', query)
    },
    onResort (list = this.list) {
      const item = this.reSortContext.item
      const fromIdx = this.reSortContext.fromIdx
      const toIdx = this.reSortContext.toIdx
      console.log(item, fromIdx, toIdx)

      this.$set(this, 'reSortContext', {
        item: null,
        fromIdx: -1,
        toIdx: -1
      })

      if (item !== null && 
          fromIdx !== -1 && 
          toIdx !== -1 &&
          fromIdx !== toIdx
      ) {
        let beforeList = list.slice(0, fromIdx)
        let afterList = list.slice(fromIdx + 1)

        if (fromIdx > toIdx) {
          beforeList.splice(toIdx, 0, item)
        } else {
          afterList.splice(toIdx - fromIdx, 0, item)
        }

        console.log(beforeList.concat(afterList))
        this.$emit('resort', beforeList.concat(afterList))

        return beforeList.concat(afterList)
      } 

      return list
    },
    $_zoomIn () {
      this.$_zoom(this.zoomSize + 1)
    },
    $_zoomOut () {
      this.$_zoom(this.zoomSize - 1)
    },
    $_zoom (size) {
      this.zoomSize = Math.min(this.zoomMax, Math.max(1, size))
    }
  }
}