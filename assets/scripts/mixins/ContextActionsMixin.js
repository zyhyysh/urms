const EnumContextActions = {
  SAVE: 0,
  PUBLISH: 1,
  PREVIEW: 2,
  RELOAD: 3,
  DELETE: 4
}

export default {
  data: () => ({
    caSchemaName: '',
    caEntity: null,
    caEntityUid: null,
    caSavable: false,
    caPublishable: false,
    caPreviewable: false,
    caReloadable: false,
    caDeletable: false
  }),
  computed: {
    contextActions () {
      return {
        save: this.caSavable ? () => this.$_callFunc('onCaSave') : undefined,
        publish: this.caPublishable ? () => this.$_callFunc('onCaPublish') : undefined,
        preview: this.caPreviewable ? () => this.$_callFunc('onCaPreview') : undefined,
        reload: this.caReloadable ? () => this.$_callFunc('onCaReload') : undefined,
        delete: this.caDeletable ? () => this.$_callFunc('onCaDelete') : undefined
      }
    }
  },
  watch: {
    contextActions: {
      deep: true,
      handler (v) {
        this.$store.dispatch('admin/registerContextActions', v)
      }
    }
  },
  methods: {
    async $_callFunc (name, args) {
      if (typeof this[name] === 'function') {
        await this[name].apply(this, args)
      }
    }
  }
}