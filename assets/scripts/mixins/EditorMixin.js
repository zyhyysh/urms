import utils from '~~/assets/scripts/utils'
import EntityMixin from '~~/assets/scripts/mixins/EntityMixin'

export default {
  mixins: [
    EntityMixin
  ],
  props: {
    /*
    ** Entity Object(Array, String, Number, Boolean, etc.)
    */
    value: {
      type: [Object, Array, String, Number, Boolean, null],
      require: true
    },
    title: {
      type: [String, Number]
    },
    /*
    ** Type of current Content
    */
    contentName: {
      type: String
    },
    disabled: {
      type: Boolean,
      default: false
    }
  },
  data: () => ({
    needCheckValid: false,
    isValid: true
  }),
  computed: {
    /*
    ** For ObjectId Reference
    */
    isObjRef () {
      return this.schemaType === 'ObjectId' && 'ref' in this.schema
    },
    hasCustomEditor () {
      return this.$hasCustomEditor(this.schema.ref)
    },
    /*
    ** for dom
    */
    editorClass () {
      const blockClass = 'entity-editor'
      const classes = [blockClass]

      if (this.hasCustomEditor) {
        classes.push(`${blockClass}--custom`)
      }

      classes.push(`${blockClass}--${this.schemaType.toLowerCase()}`)

      if (this.isRequired) {
        classes.push('entity-editor--required')
      }

      if (!this.isValid) {
        classes.push('entity-editor--invalid')
      }

      return classes.join(' ')
    },
    inputValue: {
      get () {
        return this.value
      },
      set (v) {
        this.$emit('input', v)

        if (!this.isValid) {
          this.$nextTick(() => {
            this.checkValid()
          })
        }
      }
    },
    isRequired () {
      return this.schema.required || false
    },
    // isValid () {
    //   if (this.needCheckValid) {
    //     switch (this.schemaType) {
    //       case 'String': {
    //         let valid = true
    //         if (this.isRequired) {
    //           valid = this.inputValue !== null && this.inputValue !== undefined && this.inputValue.length > 0
    //         } 
    //         if (this.schema.minlength) {
    //           valid = valid && this.inputValue.length >= Number(this.schema.minlength)
    //         }
    //         if (this.schema.maxlength) {
    //           valid = valid && this.inputValue.length <= Number(this.schema.maxlength)
    //         }
    //         if (this.schema.match && 
    //             this.schema.match.length > 0) {
    //           const r = new RegExp(this.schema.match)
    //           valid = valid && r.test(this.inputValue)
    //         }

    //         return valid
    //       }
    //       case 'Number': {
    //         let valid = true
    //         if (this.isRequired) {
    //           valid = this.inputValue !== null && this.inputValue !== undefined && this.inputValue.length > 0
    //         }
    //         if (this.schema.min) {
    //           valid = valid && Number(this.inputValue) >= Number(this.schema.min)
    //         }
    //         if (this.schema.max) {
    //           valid = valid && Number(this.inputValue) <= Number(this.schema.max)
    //         }

    //         return valid
    //       }
    //       case 'Date': {
    //         let valid = true
    //         if (this.isRequired) {
    //           valid = this.inputValue !== null && this.inputValue !== undefined && this.inputValue.length > 0
    //         }
    //         // TODO
    //       }
    //       case 'Buffer': {
    //         return true
    //       }
    //       case 'Boolean': {
    //         return !this.isRequired || this.inputValue !== null && this.inputValue !== undefined
    //       }
    //       case 'Mixed': {
    //         return true
    //       }
    //       case 'ObjectId': {
    //         return !this.isRequired || this.inputValue !== null && this.inputValue !== undefined && this.inputValue.length > 0
    //       }
    //       case 'Array': {
    //         // TODO
    //         return true
    //       }
    //       case 'Object': {
    //         // TODO
    //         return true
    //       }
    //       default: 
    //         return true
    //     }
    //   }

    //   return true
    // }
  },
  methods: {
    getType (obj) {
      return utils.getType(obj)
    },
    onEditRef (e) {
      const p = []
      if (this.title) {
        p.push(this.title)
      }

      this.$emit('editrefstart', p.concat(e))
    },
    checkValid () {
      // this.needCheckValid = true
      // const selfValid = (() => {
      //   switch (this.schemaType) {
      //     case 'String': {
      //       let valid = true
      //       if (this.isRequired) {
      //         valid = this.inputValue !== null && this.inputValue !== undefined && this.inputValue.length > 0
      //       } 
      //       if (this.schema.minlength) {
      //         valid = valid && this.inputValue.length >= Number(this.schema.minlength)
      //       }
      //       if (this.schema.maxlength) {
      //         valid = valid && this.inputValue.length <= Number(this.schema.maxlength)
      //       }
      //       if (this.schema.match && 
      //           this.schema.match.length > 0) {
      //         const r = new RegExp(this.schema.match)
      //         valid = valid && r.test(this.inputValue)
      //       }

      //       return valid
      //     }
      //     case 'Number': {
      //       let valid = true
      //       if (this.isRequired) {
      //         valid = this.inputValue !== null && this.inputValue !== undefined && this.inputValue.length > 0
      //       }
      //       if (this.schema.min) {
      //         valid = valid && Number(this.inputValue) >= Number(this.schema.min)
      //       }
      //       if (this.schema.max) {
      //         valid = valid && Number(this.inputValue) <= Number(this.schema.max)
      //       }

      //       return valid
      //     }
      //     case 'Date': {
      //       let valid = true
      //       if (this.isRequired) {
      //         valid = this.inputValue !== null && this.inputValue !== undefined && this.inputValue.length > 0
      //       }
      //       // TODO
      //     }
      //     case 'Buffer': {
      //       return true
      //     }
      //     case 'Boolean': {
      //       return !this.isRequired || this.inputValue !== null && this.inputValue !== undefined
      //     }
      //     case 'Mixed': {
      //       return true
      //     }
      //     case 'ObjectId': {
      //       return !this.isRequired || this.inputValue !== null && this.inputValue !== undefined && this.inputValue.length > 0
      //     }
      //     case 'Array': {
      //       // TODO
      //       return true
      //     }
      //     case 'Object': {
      //       // TODO
      //       return true
      //     }
      //     default: 
      //       return true
      //   }
      // })()

      if (this.isRequired && (this.inputValue === null || this.inputValue === undefined)) {
        this.isValid = false
      }
      // console.log(this.$children)
      return this.$children.reduce((v, c) => {
        if ('checkValid' in c && typeof c.checkValid === 'function') {
          return v && c.checkValid()
        }

        return v
      }, this.isValid)
    },
    confirmDelete () {
      return window.confirm('Delete this entity?')
    }
  }
}