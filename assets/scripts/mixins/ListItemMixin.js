import EntityMixin from '~~/assets/scripts/mixins/EntityMixin'

export default {
  mixins: [
    EntityMixin
  ],
  props: {
    entity: {
      type: [Object, String, Array, Number, Boolean],
      require: true
    },
    schema: {
      type: Object,
      default: () => ({
        type: undefined
      })
    },
    selected: {
      type: Boolean,
      default: false
    },
    fnPick: {
      type: Boolean,
      default: false
    }
  },
  computed: {
    entityItemClass () {
      const base = ['entity-item']

      if (this.selected) {
        base.push('entity-item--selected')
      }

      return base.join(' ')
    }
  },
  methods: {
    selectSelf () {
      this.$emit('select', this.entity)
    },
    updateSelf () {
      this.$emit('update', this.entity)
    },
    deleteSelf () {
      this.$emit('delete', this.entity)
    }
  }
}