export default {
  getType (obj) {
    return toString.call(obj).slice(8, -1).toLowerCase()
  },
  getAbsUrl (url) {
    return `/admin/${url}`
  },
  schemaIgnore(key) {
    return ['comment', 'commentx', 'embed', 'embeded', 'editor'].indexOf(key) !== -1
  },
  getDefault (schema) {
    console.log('getDefault', schema)
    const self = this
    // return (function f (schema) {
    //   return Object.keys(schema).reduce(function i (obj, key) {
    //     if (self.schemaIgnoreKeys.indexOf(key) === -1) {
    //       return obj
    //     } else {
    //       const type = 'type' in schema[key] ? ( self.getType(schema[key].type) === 'array' ? 'Array' : schema[key].type) : 'Object'

    //       if (type === 'Object') {
    //         obj[key] = f(schema[key])
    //       } else if (type === 'Array') {
    //         console.log('-----------------------------------------------------')
    //         console.log('schema', schema)
    //         console.log('+++')
    //         console.log('schema type 0', schema[key].type[0])
    //         obj[key] = []
    //       } else {
    //         obj[key] = null
    //       }

    //       return obj
    //     }
    //   }, {})
    // })(schema)

    if ('type' in schema) {
      console.log('is item', schema.type)
      if (self.getType(schema.type) === 'array') {
        return []
      } else {
        return null
      }
    } else {
      // console.log('is obj', Object.keys(schema), self.schemaIgnoreKeys)
      return Object.keys(schema).filter(key => {
        // console.log(self.schemaIgnoreKeys.indexOf(key))
        return !self.schemaIgnore(key)
      }).reduce((obj, key) => {
        console.log('reduce', obj, key)
        const subSchema = schema[key]
        obj[key] = self.getDefault(subSchema)
        return obj
      }, {})
    }
  },
  getPlainObject (obj) {
    let self = this
    return (function s (v) {
      if (self.getType(v) === 'object') {
        return Object.keys(v).reduce((obj, k) => {
          obj[k] = s(v[k])
          return obj
        }, {})
      } else if (self.getType(v) === 'array') {
        return v.slice().map(sub => {
          return s(sub)
        })
      } else {
        return v
      }
    })(obj)
  },
  getReadableSize (size) {
    let s = size
    let t = 0
    let p = ['Byte', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB']

    while (s > 1000) {
      s = s / 1000
      t += 1
    }

    s = Math.ceil(s * 10) / 10

    return `${s}${p[t]}`
  }
}