const path = require('path')
const methods = require('methods')
const pathToRegExp = require('path-to-regexp')


const Route = function (method, url, middleware, options) {
  this.options = options || {}
  this.method = method.toUpperCase()
  this.paramNames = []
  this.regexp = pathToRegExp(url, this.paramNames, this.options)
}

Route.prototype.match = function (method, url) {
  return this.method === method.toUpperCase() && this.regexp.test()
}

const router = module.exports = function (options) {
  options = options || {}
  this.prefix = options['prefix'] || ''
  this.middlewares = []

  function matches (ctx, method) {
    if (!method) { return true }
    if (ctx.method === method) { return true }
    if (method === 'GET' && ctx.method === 'HEAD') { return true }
    return false
  }

  return this
}

router.prototype.routes = function (ctx, next) {
    this.middleware.filter(m => {
      return m.match(ctx.method, ctx.path)
    })
  }

methods.forEach(m => {
  router.prototype[m] = function (url, middleware) {
    router.middlewares.push({
      method: m,
      regexp: pathToRegExp(path.join(router.prefix, url)),
      handler: middleware
    })

    return this
  }
})