const mongoose = require('mongoose')
const methods = require('methods')
const pathToRegExp = require('path-to-regexp')
const _ = require('lodash')

const modificationPlugin = require('./schemaPluginModifications.js')

mongoose.Promise = global.Promise

const defaultData = {
  tag: [
    {
      name: 'sys' 
    },
    {
      name: 'admin',
      tags: ['sys']
    }
  ],
  user: [
    {
      name: 'admin'
    }
  ]
}

class Resource {
  constructor (name, option, plugin) {
    this.option = option || { schema: {}, api: {} }
    this.plugin = plugin || { schema: {}, api: {} }
    // console.log(option)
    console.log(`init Resource ${name}`)
    let schema = (function removeComment (schema) {
      return Object.keys(schema).reduce((obj, key) => {
        if (key !== 'comment' && key !== 'editor') {
          obj[key] = schema[key]
        }

        if (toString.call(obj[key]).slice(8, -1).toLowerCase() === 'object') {
          obj[key] = removeComment(obj[key])
        } else if (toString.call(obj[key]).slice(8, -1).toLowerCase() === 'array') {
          obj[key] = obj[key].map(removeComment)
        }

        return obj
      }, {})
    })(option.schema)


    try {
      this.schema = new mongoose.Schema(schema)
      this.schema.plugin(modificationPlugin)

      if (typeof this.plugin.schema === 'function') {
        this.schema.plugin(plugin.schema)
      }

      this.model = mongoose.model(name, this.schema)
    } catch (e) {
      throw e
    }

    return this
  }
  get allowMethods () {
    return methods.filter(method => {
      if (this.option.api && method in this.option.api && this.option.api[method].allow) {
        return true
      }

      return false
    })
  }
  get accessibility () {
    return Object.keys(this.option.api).reduce((obj, key) => {
      obj[key] = this.option.api[key].accessible || 0

      return obj
    }, {})
  }
  get populateKeys () {
    let self = this
    const base = [
      {
        path: 'meta.modifications',
        options: { limit: 10 }
      },
      {
        path: 'meta.modifications.operator', 
        select: 'name'
      }
    ]

    const arr = (function findKeys (s) {
      if ('type' in s) {
        if (toString.call(s.type).slice(8, -1).toLowerCase() === 'array') {
          // return s.type.map(i => findKeys(i))
          if (s.type.length > 0) {
            return findKeys(s.type[0])
          } else {
            return false
          }
        } else if (s.type === 'ObjectId' &&
                   'ref' in s &&
                   'embed' in s &&
                   s.embed) {
          return true
        }

        return false
      } else {
        return Object.keys(s).reduce((result, k) => {
          if (k !== 'comment' && k !== 'editor') {
            const sub = s[k]
            let res = findKeys(sub)
            // let result = findKeys(sub)
            if (res) {
              if (toString.call(res).slice(8, -1).toLowerCase() === 'array') {
                res = [k].concat(res)
              } else {
                res = [k]
              }

              if (toString.call(result).slice(8, -1).toLowerCase() === 'array') {
                result.push(res)
              } else {
                result = [res]
              }
            }
          }

          return result
        }, false)
      }
    })(this.option.schema)

    if (arr) {
      return base.concat(arr.map(i => _.flattenDeep(i).join('.')))
    } else {
      return base
    }
  }
  _hasPlugin(name) {
    return name in this.plugin.api && typeof this.plugin.api[name] === 'function'
  }
  _registerMiddlewares () {
    // console.log('_registerMiddlewares')
    // this.schema.pre('save', function (next, ctx) {
    //   console.log('pre save', ctx)
    //   this.meta.modifications.push({
    //     operator: 'ctx.state.user',
    //     time: Date.now()
    //   })
    //   // this.update({}, { $add: { modifications: { operator: '', time: Date.now() } } })
    //   next()
    // })
  }
  _allowMethod (method) {
    return this.allowMethods.includes(method)
  }
  _notAllow () {
    return {
      status: 403,
      data: {
        error: 'Not Allow'
      }
    }
  }
  _notFound () {
    return {
      status: 404,
      data: {
        error: 'Not Found'
      }
    }
  }
  _error (e) {
    return {
      status: 400,
      data: e.errors ? e.errors.cover : e
    }
  }
  _response (data) {
    return {
      status: 200,
      data
    }
  }
  _pushModification(doc, ctx) {
    doc.lastModification = {
      operator: ctx.state.user ? ctx.state.user._id : null,
      time: Date.now()
    }

    return doc
  }
  async handler (ctx, something, next) {
    const method = ctx.method.toLowerCase()
    const accessible = this.accessibility[method]
    const role = ctx.state.user ? ctx.state.user.role : 3

    if (role <= accessible) {
      switch (method) {
        case 'get': {
          if (something.length === 0) {
            return await this.index(ctx)
          } else {
            return await this.show(ctx, something, next)
          }
        }
        case 'post': {
          return await this.create(ctx)
        }
        case 'patch': {
          return await this.update(ctx, something, next)
        }
        case 'put': {
          return await this.replace(ctx, something, next)
        }
        case 'delete': {
          return await this.delete(ctx, something, next)
        }
        default: {
          return this._notFound()
        }
      }
    } else {
      return this._notAllow()
    }
  }
  async index (ctx) {
    if (!this._allowMethod('get')) {
      return this._notAllow()
    }

    try {
      let query = ctx.query

      if (this._hasPlugin('parseQuery')) {
        query = this.plugin.api.parseQuery.call(this, ctx)
      }

      let q = this.model.find(query)

      q = ((query, keys) => {
        keys.forEach(k => {
          query.populate(k)
        })

        return query
      })(q, this.populateKeys)

      let r = await q.exec()

      if (this._hasPlugin('afterIndex')) {
        r = await this.plugin.api.afterIndex.call(this, ctx, r)
      }

      if (this._hasPlugin('beforeResponse')) {
        r = await Promise.all(r.map(i => {
          return this.plugin.api.beforeResponse.call(this, i)
        }))
      }

      return this._response(r)
    } catch (e) {
      return this._error(e)
    }
  }
  async show (ctx, something) {
    if (!this._allowMethod('get')) {
      return this._notAllow()
    }

    // const id = params.id
    /* TODO: find by index key */
    let q = this.model.findById(something)

    q = ((query, keys) => {
      keys.forEach(k => {
        query.populate(k)
      })

      return query
    })(q, this.populateKeys)
    let r = await q.exec()

    if (this._hasPlugin('beforeResponse')) {
      r = await this.plugin.api.beforeResponse.call(this, r)
    }

    return this._response(r)
  }
  async create (ctx) {
    if (!this._allowMethod('post')) {
      return this._notAllow()
    }

    try {
      let body = ctx.request.body

      if (this._hasPlugin('beforeCreate')) {
        body = await this.plugin.api.beforeCreate.call(this, ctx)
      }

      let m = await this.model.create(body)

      this._pushModification(m, ctx)

      let r = await m.save()

      if (this._hasPlugin('beforeResponse')) {
        r = await this.plugin.api.beforeResponse.call(this, r)
      }

      return this._response(r)
    } catch (e) {
      console.error(e)
      return this._error(e)
    }
  }
  async update (ctx, something) {
    let doc = await this.model.findById(something).exec()

    if (!doc) {
      return this._notFound()
    }

    if (this._hasPlugin('beforeUpdate')) {
      try {
        doc = await this.plugin.api.beforeUpdate.call(this, ctx, something, doc)
      } catch (e) {
        console.error(e)
        return this._error(e)
      }
    }

    try {
      Object.keys(ctx.request.body).forEach(k => {
        doc[k] = ctx.request.body[k]
      })
      this._pushModification(doc, ctx)
      const r = await doc.save()

      if (this._hasPlugin('beforeResponse')) {
        r = await this.plugin.api.beforeResponse.call(this, r)
      }

      return this._response(r)
    } catch (e) {
      console.error(e)
      return this._error(e)
    }
  }
  async replace (ctx, something) {
    let doc = await this.model.findById(something).exec()   

    if (!doc) {
      return this._notFound()
    } 

    if (this._hasPlugin('beforeReplace')) {
      try {
        doc = await this.plugin.api.beforeReplace.call(this, ctx, something, doc)
      } catch (e) {
        console.error(e)
        return this._error(e)
      }
    }

    try {
      const meta = this._pushModification(doc, ctx).meta
      const newDoc = Object.assign(ctx.request.body, {meta})

      let r = await doc.update(newDoc)

      if (r.ok) {
        r = await this.model.findById(something).exec()
        r = await r.save()
      }

      if (this._hasPlugin('beforeResponse')) {
        r = await this.plugin.api.beforeResponse.call(this, r)
      }
      
      return this._response(r)
    } catch (e) {
      return this._error(e)
    }
  }
  async delete ({ request }, something) {
    const r = await this.model.deleteOne({ _id: something })

    return this._response(r)
  }
}

module.exports = Resource