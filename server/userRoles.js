module.exports = {
  roles: {
    nobody: 0,
    admin: 1,
    author: 2,
    anyone: 3
  }
} 