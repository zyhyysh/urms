module.exports = exports = function modificationPlugin (schema, options) {
  schema.add({
    meta: {
      draft: {
        type: Boolean,
        default: true
      },
      modifications: {
        type: [{
          operator: {
            type: 'ObjectId',
            ref: 'user'
          },
          time: {
            type: Date,
            default: Date.now,
            required: true
          }
        }],
        capacity: 1000
      }
    }
  })

  const lastModification = schema.virtual('lastModification')
  lastModification.get(function () {
    return this.meta.modifications[this.meta.modifications.length - 1]
  })

  lastModification.set(function (v) {
    this.meta.modifications.push(v)
  })

}