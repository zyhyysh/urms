const path = require('path')

const Koa = require('koa')
const koaBody = require('koa-body')
const mount = require('koa-mount')

const { Nuxt, Builder } = require('nuxt')

// const Admin = require('./admin.js')
const Api = require('./api/index.js')
const ResourceManager = require('./ResourceManager.js')
const AliOSS = require('./aliyunoss.js')

let serverConfig = require('../server.config.js')
// Import and Set Nuxt.js options
let config = require('../nuxt.config.js')

async function start() {
  const app = new Koa()
  const host = process.env.HOST || '127.0.0.1'
  const port = process.env.PORT || serverConfig.port || 3333

  app.use(async (ctx, next) => {
    ctx.serverConfig = serverConfig
    await next()
  })

  app.use(koaBody())

  // const api = await Api(serverConfig)

  app.use(AliOSS(serverConfig.alioss))

  app.use(mount('/api', await Api(serverConfig)))
  
  config.dev = !(app.env === 'production')
  // Instantiate nuxt.js
  const nuxt = new Nuxt(config)

  // Build in development
  if (config.dev) {
    const builder = new Builder(nuxt)
    await builder.build()
  }

  app.use(async (ctx, next) => {
    // await next()
    ctx.status = 200 // koa defaults to 404 when it sees that status is unset

    return new Promise((resolve, reject) => {
      ctx.res.on('close', resolve)
      ctx.res.on('finish', resolve)
      nuxt.render(ctx.req, ctx.res, promise => {
        // nuxt.render passes a rejected promise into callback on error.
        promise.then(resolve).catch(reject)
      })
    })
  })

  app.listen(port, host)
  console.log('Server listening on http://' + host + ':' + port) // eslint-disable-line no-console
}

start()
