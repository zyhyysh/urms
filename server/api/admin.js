const crypto = require('crypto')
const jwt = require('jsonwebtoken')
const Router = require('koa-tree-router')
// const ResourceModel = require('../ResourceModel')

const roles = require('../userRoles').roles

const router = new Router({
  prefix: '/admin',
})

module.exports = createAdmin = (rManager) => {

  // router.get('/ass', async (ctx) => {
  //   try {
  //     const c = await aliyunoss()

  //     if (c && 'AccessKeyId' in c) {
  //       ctx.body = c
  //     }
  //   } catch (e) {
  //     ctx.status = 405
  //     ctx.body = 3
  //   }
  // })

  router.get('/auth', async (ctx) => {
    const user = ctx.state.user

    if (user) {
      delete user.hash
      delete user.pass

      ctx.body = {
        user
      }

    } else {
      ctx.status = 500
      ctx.body = {
        error: 'Invalid token'
      }
    }
  }).post('/auth', async (ctx) => {
    const name = ctx.request.body.username
    const pass = ctx.request.body.password

    if (name && name.length > 0 && pass && pass.length > 0) {
      let user = await rManager.resources.user.model.findOne({ name }).exec()
      // console.log(Object.keys(user), user.pass === pass)

      const hash = crypto.createHash('sha256')
      const passHash = hash.update(pass)
      const passHex = passHash.digest('hex')

      // console.log(user.hash,  .passHex)

      if (user && user.hash === passHex) {

        const userObject = user.toObject()

        delete userObject.pass

        const token = await (new Promise((resolve, reject) => {
          jwt.sign({ ...userObject }, 'shared-secret', {
            expiresIn: '8h'
          }, (err, token) => {
            if (err) {
              reject(err)
            }

            resolve(token)
          })
        }))

        // try {
        //   const c = await aliyunoss()
        //   userObject['token'] = token
        //   userObject['alioss'] = c

        //   ctx.status = 200
        //   ctx.body = userObject

        //   return
        // } catch (e) {
        //   ctx.status = 500
        //   ctx.body = e

        //   return
        // }

        userObject['token'] = token

        ctx.status = 200
        ctx.body = userObject
        return
      }
    }

    ctx.status = 401
    ctx.body = {
      error: 'Invalid username or password'
    }
  }).delete('/auth', (ctx) => {
    ctx.body = ''
  })

  function checkAuth (role) {
    return async (ctx, next) => {
      if (ctx.state.user && ctx.state.user.role <= roles[role]) {
        await next()
      } else {
        ctx.status = 405
        ctx.body = {
          error: 'Not Allow'
        }
      }
    }
  }

  router.get('/model', checkAuth('author'), (ctx) => {
    const query = ctx.request.query
    const showSystem = query['system'] || false

    // ctx.body = rManager.getList(showSystem, showDeleted)

    if ('deleted' in query && Boolean(query['deleted'])) {
      ctx.body = Object.keys(rManager.options).filter(key => {
        return rManager.options[key].meta.deleted
      })
    } else {
      ctx.body = rManager.getList(showSystem)
    }
  }).get('/model/:name', checkAuth('author'), (ctx) => {
    const name = ctx.params.name
    ctx.body = rManager.resources[name].option
  }).post('/model', checkAuth('admin'), async (ctx) => {
    const name = ctx.request.body.name

    if (!name) {
      ctx.status = 500
      ctx.body = {
        error: 'Name Required'
      }
    } else if (name in rManager.resources) {
      ctx.status = 500
      ctx.body = {
        error: 'Duplicate Name'
      }
    } else {
      try {
        await rManager.create(name)
        ctx.body = 'success'
      } catch (e) {
        console.error(e)
        ctx.status = 500
        ctx.body = {
          error: e.errors.cover
        }
      }
    }
  }).put('/model/:name', checkAuth('admin'), async (ctx) => {
    const name = ctx.params.name
    const option = ctx.request.body.option

    console.log('[put /model/:name]')

    if (!(name in rManager.resources)) {
      ctx.status = 404
      ctx.body = {
        error: 'Not Found'
      }
    } else {
      try {
        await rManager.update(name, option)
        console.log(rManager.list)
        ctx.body = 'success'
      } catch (e) {
        ctx.status = 500
        ctx.body = {
          error: e.errors ? e.errors.cover : e
        }
      }
    }
  }).delete('/model/:name', checkAuth('admin'), async (ctx) => {
    const name = ctx.params.name
    console.log('delete', name)

    if (!(name in rManager.resources)) {
      ctx.status = 404
      ctx.body = {
        error: 'Not Found'
      }
    } else {
      try {
        await rManager.delete(name)

        ctx.body = 'success'
      } catch (e) {
        ctx.status = 500
        ctx.body = {
          error: e.errors.cover
        }
      }
    }
  })

  return router.routes()
}

