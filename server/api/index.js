const Koa = require('koa')
const koaBody = require('koa-body')
const jwt = require('koa-jwt')

const admin = require('./admin.js')
const ResourceManager = require('../ResourceManager.js')

module.exports = createApi = async (options) => {
  const api = new Koa()
  const rm = new ResourceManager(options)
  await rm.registerDefaultData()

  // api.use(koaBody())
  api.use(jwt({ secret: 'shared-secret', passthrough: true }))
  api.use(rm.middleware())
  api.use(admin(rm))
  api.use((ctx) => {
    ctx.status = 404
    ctx.body = {
      error: 'Api Not Found'
    }
  })

  return api
}