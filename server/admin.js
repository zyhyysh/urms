const Koa = require('koa')
const mount = require('koa-mount')

const { Nuxt, Builder } = require('nuxt')

let nuxtConfig = require('../nuxt.config.js')

module.exports = startAdmin = async function () {
  const admin = new Koa()

  nuxtConfig.dev = !(admin.env === 'production')
  // nuxtConfig.env = { baseUrl: 'http://127.0.0.1:3333/admin' }
  // nuxtConfig.router ? nuxtConfig.router.base = '/admin/' : nuxtConfig.router = { base: '/admin/'}
  // console.log(process.env.BASE_URL)

  const nuxt = new Nuxt(nuxtConfig)

  if (nuxtConfig.dev) {
    const builder = new Builder(nuxt)
    await builder.build()
  }

  admin.use(async (ctx, next) => {
    await next()
    ctx.status = 200 // koa defaults to 404 when it sees that status is unset
    return new Promise((resolve, reject) => {
      ctx.res.on('close', resolve)
      ctx.res.on('finish', resolve)
      nuxt.render(ctx.req, ctx.res, promise => {
        // nuxt.render passes a rejected promise into callback on error.
        promise.then(resolve).catch(reject)
      })
    })
  })

  return admin
}

