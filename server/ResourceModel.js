const path = require('path')
const fs = require('fs')

let defaultApi = {
  get: { allow: false },
  post: { allow: false },
  delete: { allow: false },
  put: { allow: false },
  patch: { allow: false }
}

class ResourceModel {
  constructor (name, schema = {}, api = defaultApi) {
    this.name = name
    this.schema = schema
    this.api = api
  }
  get option () {
    return {
      schema: this.schema,
      api: this.api
    }
  }
  saveOptionFile () {

  }

}

module.exports = ResourceModel