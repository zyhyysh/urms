const mongoose = require('mongoose')
const glob = require('glob')
const path = require('path')
const Router = require('koa-trie-router')
const fs = require('fs')

mongoose.Promise = global.Promise

const router = new Router()

// const prototype = {
//   model: 'Mixed',
//   api: {
//     get: {
//       allow: 'Boolean',
//       populateKeys: ['String']
//     },
//     post: {
//       allow: 'Boolean'
//     },
//     del: {
//       allow: 'Boolean'
//     },
//     put: {
//       allow: 'Boolean'
//     },
//     patch: {
//       allow: 'Boolean'
//     }
//   }
// }

// const resMetaModel = {
//   updations: {
//     type: [{
//       type: 'ObjectId',
//       ref: 'updations'
//     }]
//   },
//   owner: {
//     type: 'ObjectId',
//     ref: 'user'
//   },
//   mode: {
//     type: 'String',
//     default: '664'
//   }
// }

const middleware = module.exports = {
  /* mongoose models */
  models: {},
  /* prototypes { schema model, api option } */
  prototypeOptions: {},
  optionPath: '',
  /* Register schema to mongoose and generate router */
  loadPrototype (name, option) {
    console.log('loadPrototype', name, option)
    this.prototypeOptions[name] = option
    this.models[name] = mongoose.model(name, mongoose.Schema(option.model))
    console.log(Object.keys(this.models))

    if (option.api) {
      const url = path.join('/api', name)

      /* GET */
      if (option.api.get.allow) {
        router
        .get(url, async (ctx, next) => {
          let qs = ctx.query
          let query = ctx.model[name].find(qs)

          if (option.api.get.populateKeys && typeof option.api.get.populateKeys.map === 'function') {
            option.api.get.populateKeys.map(key => {
              console.log('populating', key)
              query = query.populate({ path: key })
            })
          }

          ctx.body = await query.exec()
        })
        .get(path.join(url, '/:id'), async (ctx, next) => {
          const id = ctx.params.id
          let query = ctx.model[name].findById(id)

          ctx.body = await query.exec()
        })
      }

      /* POST */
      if (option.api.post.allow) {
        router.post(url, async (ctx, next) => {
          const M = ctx.model[name]

          try {
            const m = await M.create(ctx.request.body)
            ctx.status = 201
            ctx.body = r
          } catch (e) {
            ctx.body = e
          }
        })
      }
    }
  },
  /* save prototype options to file */
  savePrototypeFile (name, options) {
    return new Promise((resolve, reject) => {
      const fileData = JSON.stringify(options)
      // console.log('[savePrototypeFile]', url, options, this.optionPath)
      // resolve(1)
      fs.writeFile(path.join(this.optionPath, `${name}.json`), fileData, err => {
        console.log('in savePrototypeFile', err)
        if (err) {
          reject(err)
        } else {
          resolve(fileData)
        }
      })
    })
  },
  /* delete prototype file */
  removePrototypeFile (name) {
    return new Promise((resolve, reject) => {
      fs.unlink(path.join(this.optionPath, `${name}.json`), err => {
        if (err) reject(err)
        resolve()
      })
    })
  },
  _reloadFn: function () {},
  init (options) {
    mongoose.connect(`mongodb://${options.host}:${options.port}/${options.database}`)

    // resource model
    // this.models['model'] = mongoose.model('model', mongoose.Schema(prototype))

    // schema
    if (options.schema) {
      const schema = options.schema + (options.schema.lastIndexOf('/') === (options.schema.length - 1) ? '' : '/')
      const spath = path.join(__dirname, schema)
      const files = glob.sync(spath + '/**/*.json')

      this.optionPath = spath

      files.map(file => {
        // console.log(file, spath)
        const name = file.replace(spath, '')
                          .replace(/\.json$/g, '')
                          .replace(/\//g, '.')
                          .toLowerCase()
        const option = require(file)
        // if (!schema.subDocument) {
        this.loadPrototype(name, option)
        // }
      })

      router
      .get('/api/model', (ctx, next) => {
        ctx.body = Object.keys(this.prototypeOptions)
      })
      .get('/api/model/:name', (ctx, next) => {
        const name = ctx.params.name

        if (name in this.prototypeOptions) {
          ctx.body = this.prototypeOptions[name]
        } else {
          ctx.status = 404
        }
      })
      .post('/api/model', async (ctx, next) => {
        const name = ctx.request.body.name
        const model = ctx.request.body.model
        const api = ctx.request.body.api || {
          get: { allow: false },
          post: { allow: false },
          del: { allow: false },
          put: { allow: false },
          patch: { allow: false }
        }

        console.log('[POST] /api/model', name, model, api)

        if (name === undefined || name in this.prototypeOptions) {
          ctx.body = 'fail: invalid name'
        } else if (model === undefined || model === null) {
          ctx.body = 'fail: invalid schema'
        } else {
          try {
            await this.savePrototypeFile(name, { model, api })
            this.loadPrototype(name, { model, api })
          } catch (e) {
            ctx.body = `fail: ${e}`
          }

          ctx.body = 'success'
        }
      })
      .put('/api/model/:name', async (ctx, next) => {
        const name = ctx.params.name
        const model = ctx.request.body.model
        const api = ctx.request.body.api || null

        if (name === undefined || !(name in ctx.model)) {
          ctx.body = 'fail: invalid name'
        } else if (model === undefined || model === null) {
          ctx.body = 'fail: invalid schema'
        } else {
          try {
            await this.savePrototypeFile(name, { model, api })
            this.loadPrototype(name, { model, api })
          } catch (e) {
            ctx.body = `fail: ${e}`
          }

          // options.onReload(router)
          ctx.body = 'success'
        }
      })
      .del('/api/model/:name', async (ctx, next) => {
        const name = ctx.params.name
        console.log('[delete]', ctx.params.name, path.join(this.optionPath, `${name}.json`))

        try {
          await this.removePrototypeFile(name)
          delete this.models[name]
          delete this.prototypeOptions[name]
        } catch (e) {
          ctx.body = `fail: ${e}`
        }

        ctx.body = 'success'
      })
    }

    return {
      async db (ctx, next) {
        ctx.model = middleware.models

        await next()
      },
      router
    }
  }
}