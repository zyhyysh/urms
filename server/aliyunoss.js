// const ALY = require('aliyun-sdk')

// const sts = new ALY.STS({
//   accessKeyId: 'LTAIy4uHaqHpJDS2',
//   secretAccessKey: '4soQYied3O9FlJC3tgbU7r4yMqWwlL',
//   endpoint: 'https://sts.aliyuncs.com',
//   apiVersion: '2015-04-01'
// })

// sts.assumeRole({
//   Action: 'AssumeRole',
//   RoleArn: 'acs:ram::1195610621755522:role/test-urms-oss',
//   DurationSeconds: 3600,
//   RoleSessionName: ''
// }, (err, res) => {

// })
const co = require('co')

const { STS } = require('ali-oss')

// const server = {
//   endpoint: 'oss-cn-beijing.aliyuncs.com',
//   bucket: 'test-urms'
// }

module.exports = function (config) {
  const client = new STS({
    accessKeyId: config.accessKeyId,
    accessKeySecret: config.accessKeySecret
  })

  return async (ctx, next) => {
    ctx.signAliOSS = async () => {
      return (new Promise((resolve, reject) => {
        co(function * () {
          const result = yield client.assumeRole(config.role, config.policy, 15 * 60, 'testurms')
          let obj = result.credentials
          obj['endpoint'] = config.endpoint
          obj['bucket'] = config.bucket
          resolve(obj)
        }).catch(function (err) {
          console.error('aliyun oss err', err)
          reject(err)
        })
      }))
    }
    await next()
  }
    
}