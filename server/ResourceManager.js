const path = require('path')
const fs = require('fs')

const mongoose = require('mongoose')
const glob = require('glob')
const Router = require('koa-tree-router')
const decache = require('decache')

const Resource = require('./Resource.js')
// const modificationPlugin = require('./schemaPluginModifications.js')

const defaultOption = () => {
  return {
    meta: {
      modified: Date.now(),
      created: Date.now(),
      deleted: false,
      system: false
    },
    schema: {},
    api: {
      get: { allow: false, accessible: 3 },
      post: { allow: false, accessible: 2 },
      delete: { allow: false, accessible: 2 },
      put: { allow: false, accessible: 2 },
      patch: { allow: false, accessible: 1 }
    }
  }
}

class ResourceManager {
  constructor (option) {
    this.serverOption = Object.assign({ api: { prefix: '/resource' } }, option)
    this.optionAbsPath = path.join(__dirname, option.api.resourceOptions || './options')
    this.pluginAbsPath = path.join(__dirname, option.api.resourcePlugins || './plugins')
    this.resources = {}
    this.options = {}
    this.plugins = {}

    this.router = new Router({
      prefix: option.api.prefix,
      onMethodNotAllowed: ctx => {
        ctx.body = {
          error: 'Not Allowed'
        }
      }
    })

    try {
      const db = this.serverOption.db
      mongoose.connect(`mongodb://${db.host}:${db.port}/${db.database}`)
      // mongoose.plugin(modificationPlugin)

      glob.sync(path.join(this.optionAbsPath, '/**/*.json')).map(file => {
        const name = path.relative(this.optionAbsPath, file)
                     .replace(/\.json$/g, '')
                     .replace(/\//g, '.')
                     .toLowerCase()
        this.loadFromFile(name, path.extname(file))
      })
    } catch (e) {
      console.error('[Api cconstructor] create Api error')
      throw e
    }
  }
  getList (showSystem = false) {
    return Object.keys(this.resources).filter(k => {
      const obj = this.options[k]
      let f = true
      if (!showSystem) {
        f = f && !('meta' in obj && obj.meta.system)
      }

      return f
    }).reduce((obj, key) => {
      let item = this.options[key].meta
      // delete item.api
      // delete item.schema
      obj[key] = item

      return obj
    }, {})
  }
  async registerDefaultData () {
    /* default users */
    const nobody = await this.resources.user.model.findOne({ name: 'nobody' }).exec()
    if (!nobody) {
      const nobody = await this.resources.user.model.create({ name: 'nobody', pass: '!@#$%^', email: 'zyhyysh@gmail.com', role: '0' })
      
      if (nobody) {
        return true
      } else {
        return false
      }
    }

    return true
  }
  async create (name, option = defaultOption()) {
    try {
      await this.writeToFile(name, option)
      this.loadFromFile(name)
    } catch (e) {
      throw e
    }
  }
  async update (name, option) {
    // const currentOption = this.options[name]
    // let current Meta = option.meta
    option.meta.modified = Date.now()
    // option.meta = currentMeta
    try {
      await this.writeToFile(name, option)
      await this.deleteResourceModel(name)
      this.loadFromFile(name)
    } catch (e) {
      throw e
    }
  }
  writeToFile(name, option) {
    return new Promise((resolve, reject) => {
      const data = JSON.stringify(option)
      const file = path.join(this.optionAbsPath, `${name}.json`)
      console.log('[fn] writeToFile', name)
      fs.writeFile(file, data, err => {
        if (err) {
          reject(err)
        } else {
          resolve(data)
        }
      })
    })
  }
  loadFromFile (name, extname) {
    const file = path.join(this.optionAbsPath, `${name}.json`)
    const option = require(file)

    const pluginPath = path.join(this.pluginAbsPath, `${name}/plugin.js`)
    const pluginExist = this.plugins[name] || fs.existsSync(pluginPath)
    console.log('[fn] loadFromFile', name, pluginExist)

    this.options[name] = option
    if (!(name in this.plugins) && pluginExist) {
      this.plugins[name] = require(pluginPath)
    }

    if (!option.meta.deleted) {
      this.resources[name] = new Resource(name, option, this.plugins[name] || null)
    }
  }
  async delete (name) {
    const option = this.options[name]
    option.meta.deleted = true
    await this.writeToFile(name, option)
    await this.deleteResourceModel(name)
  }
  deleteResourceModel (name, deleteFile = false) {
    return new Promise((resolve, reject) => {
      const file = path.join(this.optionAbsPath, `${name}.json`)
      // const resource = this.resources[name]
      delete mongoose.models[name]
      delete this.resources[name]
      decache(file)
      if (deleteFile) {
        fs.unlink(file, err => {
          if (err) reject(err)
        })
      }

      resolve()
    })
  }
  _createPrototype (name, schema, api) {

  }
  _updatePrototype () {

  }
  _removePrototype () {

  }
  /* return router middleware */
  middleware () {
    this.router.all('/*resource', async (ctx, next) => {
      const params = ctx.params.resource.split('/').slice(1)
      const name = params[0]
      const something = params.slice(1).join('/')
      const resource = this.resources[name]

      // console.log(params, name, ctx.request.body)

      if (resource) {
        const { status, data } = await resource.handler(ctx, something, next)
        ctx.status = status

        if (data) {
          ctx.body = data
        }
      } else {
        ctx.status = 404
        ctx.body = {
          error: `Resource [${params[0]}] Not Found`
        }
      }
    })

    return this.router.routes()
  }
}

module.exports = ResourceManager